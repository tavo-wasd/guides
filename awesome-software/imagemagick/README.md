### Remove background from image

Background color can be specified as hex or a name such
as `white`. The `fuzz` parameter should be modified to
fit desired output, 10% is usually a good bet.

```shell
convert -transparent "#ffffff" -fuzz 10% imagen.png imagen-sin-fondo.png
```
