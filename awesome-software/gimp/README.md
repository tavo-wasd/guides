# GIMP

The best photo editor, ever.
Free, libre, powerful, fast, lean size, constant development.

## Tips

### [PhotoGIMP by Diolinux](https://github.com/Diolinux/PhotoGIMP/)

GIMP configuration files to make it look like photoshop.
For GNU/Linux users, run this to install or update
[PhotoGIMP](https://github.com/Diolinux/PhotoGIMP/)
(it will probably overwrite pre existing configs, so,
run at your own risk):

```shell
mkdir -p ~/.config/GIMP && wget -qO- https://api.github.com/repos/Diolinux/PhotoGIMP/tarball | tar -xz -C ~/.config/GIMP/ --wildcards "*/.var/app/org.gimp.GIMP/config/GIMP/2.10" --strip-components=6
```

For the rest of the world, visit the project's repository
[here](https://github.com/Diolinux/PhotoGIMP/)
and follow installation instructions.
