# General page layout and content formatting

```groff
.ds FAM T \" H=Helvetica C=Courier
.nr PS 10 \" Point size
.nr VS 12 \" Line spacing
.nr PO 1i \" Page offset
.nr LL 6i \" Line length
.nr LT 6i \" Header/Footer length
.nr HM 1i \" Header margin
.nr FM 1i \" Footer margin
```

For example, using:

```groff
.ds FAM T \" Font family
.nr PS 12 \" Point size
.nr VS 20 \" Line length
.TL
Título
.AU
Autor
.
.LP
Lorem ipsum dolor sit amet, consectetur...
```

Produces:

![times](times.png)

... and using:

```groff
.ds FAM H \" Font family
.nr PS 12 \" Point size
.nr VS 20 \" Line length
.TL
Título
.AU
Autor
.
.LP
Lorem ipsum dolor sit amet, consectetur...
```

Produces:

![helvetica](helvetica.png)

# First page

```groff
.TL
<título>
.AU
<autor>
.AU
<otro autor>
.AI
<institución>
.
.AB
<abstract>
.AE
```

By default, groff will start the document right after
of the ABSTRACT, or if it does not exist, after the author
or the title. To use a **cover**, you can use
the following directives:

```groff
.RP no \" 'Report page'
.P1    \" Start numbering on page 1
```

# Sections and table of contents

```groff
.NH
.XN "<sección numerada>"
.
.NH 2
.XN "<sección numerada>"
.
.SH
.XN "<sección sin numerar>"
.
.TC \" TOC at the end of the document
```

By default, groff puts the table of contents at the bottom of the page.
To put it at the beginning, or after a cover page, you can use
the following macro:

```groff
.\" github.com/SudarsonNantha
.\" ---
.\" Relocate .TC at the end of the document to either 
.\" 'before' or 'after' (by giving those arguments)
.\" the page where this macro was sourced.
.\" ---
.\" Requires -mspdf macros
.
.if '\*[.T]'pdf' \X'pdf: pagename here'
.rn TC TCold
.de TC
.   ds CF
.   ds CH
.   if '\*[.T]'pdf' \X'pdf: switchtopage \\$1 here'
.   TCold
..
```

To use it, add the previous code at the beginning of the
document and when calling `.TC` at the end of the
code, you can change the directive to `.TC after`
to put the table of contents after the cover, or
`.TC before` to put it before the cover.

![toc](toc.png)

# Párrafos

```groff
.PP
<párrafo con \f[CW]sangría\f[] \m[blue]con\m[] un \f[B]poco\f[] de \f[I]formato\f[]>
.
.LP
<párrafo sin \f[CW]sangría\f[] \m[red]con\m[] un \f[B]poco\f[] de \f[BI]formato\f[]>
```

![ejemplo-parrafo](parrafo.png)

# Matemáticas

The following definitions in `eqn` allow the
Simplified use of syntax when creating equations.
(The `delim $$` works to put math syntax inside the text,
similar to LaTeX).

```groff
.EQ
delim $$
define / 'over'
define <( '{ \[la] }'
define )> '{ \[ra] }'
define grad '{ \[gr] }'
define lag '\f[U-HR]\[u2112]'
.EN
```

For example, the expression:

```groff
.EQ
\[gr] \f[U-HR]\[u2112] ( lambda , x , y ) =
    \[la]
        {partial lag} over {partial lambda} ,
        {partial lag} over {partial x} ,
        {partial lag} over {partial y}
    \[ra]
.EN
```

Can be simplified as (after applying the above definitions):

```groff
.EQ
grad lag ( lambda , x , y ) =
    <(
        {partial lag} / {partial lambda} ,
        {partial lag} / {partial x} ,
        {partial lag} / {partial y}
    )>
.EN
```

To produce:

![eqn](eqn.png)

# Other languages

groff uses variables to store the names of
the table of contents, or the references. For
change them, the variables are modified like this:

```groff
.\" Redefinición de registros comunes
.\" para títulos y fechas en español.
.
.ds ABSTRACT RESUMEN
.ds TOC Tabla de contenido
.ds REFERENCES Referencias
.
.ds MONTH1 enero
.ds MONTH2 febrero
.ds MONTH3 marzo
.ds MONTH4 abril
.ds MONTH5 mayo
.ds MONTH6 junio
.ds MONTH7 julio
.ds MONTH8 agosto
.ds MONTH9 septiembre
.ds MONTH10 octubre
.ds MONTH11 noviembre
.ds MONTH12 diciembre
.
.ds DY \n[dy] de \*[MO] de \n[year]
```

# Utilities

## Rule

```groff
.de rule
\D'l \\$1 0'
..
```

Definiendo esta macro, se puede utilizar la directiva
`.rule LONGITUD` para dibujar una línea en el documento,
por ejemplo después de un título (el espaciado es para
evitar que el título tome la longitud de la línea):

```groff
.NH
.XN "\s'20'Título de ejemplo"
.sp 0i \" Espaciado de 0
.rule 6.25i
```

El `\s'20'` pone el tamaño de letra en 20, y produce:

![ejemplo-regla](regla.png)
