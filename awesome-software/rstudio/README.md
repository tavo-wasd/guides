# Operaciones en R

Cargar archivos como `dataframes`,
donde `N_DATASET` es donde se quieren guardar
los datos, y `FILE.xlsx` el archivo en cuestión.
Es más recomendable realizar esta importación
por la interfaz gráfica que ofrece una previa y
otras opciones útiles.

```r
library(readxl)
DATASET_N <- read_excel("FILE.xlsx")
View(DATASET_N)
```

Crea una lista `DATA_LIST` con los `dataframes` importados
previamente. Luego, combinar la lista en uno solo.

```r
DATA_LIST <- list(DATASET_01, DATASET_02, ... DATASET_N)
DATA_TOT <- Reduce(function(x, y) merge(x, y, all=TRUE), DATA_LIST)
```

Para un `dataframe` `DATA_TOT`, añadir una variable (columna)
con el valor de una variable `VAR_VALUE`, en caso de ser **mayor** a cero.

```r
DATA_TOT$VAR_POSITIVE <- ifelse(DATA_TOT$VAR_VALUE > 0, DATA_TOT$VAR_VALUE, 0)
```

Para un `dataframe` `DATA_TOT`, añadir una variable (columna)
con el valor de una variable `VAR_VALUE`, en caso de ser **menor** a cero.

```r
DATA_TOT$VAR_NEGATIVE <- ifelse(DATA_TOT$VAR_VALUE < 0, DATA_TOT$VAR_VALUE, 0)
```

Para un `dataframe` `DATA_TOT`, sumar cada variable `VAR_VALUE` perteneciente
a un grupo encontrado en la variable `VAR_GROUPS`.

```r
aggregate(DATA_TOT$VAR_VALUE, by=list(Group=DATA_TOT$VAR_GROUPS), FUN=sum)
```

Para un `dataframe` `DATA_TOT`, sumar toda la columna `VAR_VALUE`.
El argumento `na.rm = TRUE` elimina las entradas con "NA".

```r
sum(as.numeric(DATA_TOT$VAR_VALUE), na.rm = TRUE)
```

Para un `dataframe` `DATA_TOT`, crear una variable `VAR_SUBSTRING`
con los dígitos desde A hasta B de la variable `VAR_STRING`.

```r
DATA_TOT$VAR_SUBSTRING <- as.numeric(substr(DATA_TOT$VAR_STRING, A, B))
```

Para un `dataframe` `DATA_TOT`, crear una variable `ISO_DATE`, utilizando una
fecha en `VAR_DATE` especificada como `format="%d-%m-%Y"`, convertida a
formato ISO.

```r
DATA_TOT$ISO_DATE <- as.Date(as.character(DATA_TOT$VAR_DATE), format="%d-%m-%Y")
```

Para un `dataframe` `DATA_TOT`, filtrar desde/hasta una fecha
(y opcionalmente desde/hasta otra fecha), y sumar solamente los
campos de la columna `VAR_VALUE`.

```r
DATA_TOT %>%
    filter(VAR_DATE < '2023-11-02') %>%
    filter(VAR_DATE > '2023-10-10') %>%
    pull(VAR_VALUE) %>%
    sum
```
