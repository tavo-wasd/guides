# Guide to configuring Debian GNU/Linux

I recommend both the GNOME and KDE desktop environments,
this guide is made around the assumption that you are using
one of those. However, this could also be used as a reference
for general tweaks & tips for any desktop environment.

## Manual update

```shell
sudo apt update && sudo apt upgrade -y
```

## Non-free repositories (for Valve's Steam, Microsoft Fonts, etc)

```shell
sudo apt-add-repository contrib non-free -y
```

### Non-free codecs & Microsoft Fonts

```shell
sudo apt install vlc libavcodec-extra ttf-mscorefonts-installer -y
```

#### (Extra) Nvidia drivers

```shell
sudo apt install nvidia-driver
```

### Steam

```shell
sudo dpkg --add-architecture i386 # Add 32bit packages
sudo apt update                   # Apply changes
sudo apt install steam-installer -y
```

Enable Proton under Settings > Compatibility > Enable Steam Play for all other titles

## Flatpak (e.g. for Discord)

Discover > Preferences > Install Flatpak

Remember flatpak is just a packaging format, [flathub](https://flathub.org) is just
a popular repository for software where you can find official
builds for [Discord](https://flathub.org/apps/com.discordapp.Discord) or [Telegram Desktop](https://flathub.org/apps/org.telegram.desktop), and unofficial builds
for [Miscrosoft Teams](https://flathub.org/apps/com.github.IsmaelMartinez.teams_for_linux) or [VSCode](https://flathub.org/apps/com.visualstudio.code).

## Custom locales.

First, look for your preferred locale in
the available locales with this command:

```shell
cat /usr/share/i18n/SUPPORTED
```

Choose the locale you prefer, and run:

```shell
# locale="CHOSENLOCALE" ; ! grep -q "^$locale" /etc/locale.gen && echo "$locale" | sudo tee -a '/etc/locale.gen'
# For EXAMPLE:
locale="es_CR.UTF-8 UTF-8" ; ! grep -q "^$locale" /etc/locale.gen && echo "$locale" | sudo tee -a '/etc/locale.gen'
```

You can add as many locales as you want.
Then, generate the locales selected by running:

```shell
sudo locale-gen
```

## Restart

You should restart your computer after applying this changes,
especially for flatpak and gpu drivers.

## Recommended software
