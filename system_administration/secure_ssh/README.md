# First login

```shell
# Login into the VPS
ssh root@codespec.xyz

# Update packages
apt update && apt upgrade

# Enable automatic updates
apt install unattended-upgrades
dpkg-reconfigure unattended-upgrades
```

# Add a new user

This is done so that you can disable
loging in as root user.

```shell
# '-m' creates a home directory,
# '-s' assigns a shell (/bin/bash in this case),
# 'user' is the name you want to give it
useradd -m -s /bin/bash user
passwd user # Set a password for 'user'
```

Add 'user' to sudo group. This will allow to execute
commands as root, such as `sudo apt update`.
Some distros user 'wheel' group instead of 'sudo'
Run command `visudo` to check root enabled groups.
Debian, Ubuntu and derivatives usually use 'sudo'.

```shell
usermod -aG sudo user   # Give 'user' sudo privileges
su - user               # Login into 'user'
sudo apt update         # Test that you have access to sudo

# Exit your server, you may need to run 'exit' twice.
exit
```

# Configure SSH keypair

## In your local machine:
```shell
ssh-keygen                                          # Generate keypair
ssh-copy-id -i ~/.ssh/id_rsa.pub user@codespec.xyz  # Copy to server
ssh user@codespec.xyz                               # Check it works
```

## ONCE IN THE VPS:
`sudo nano /etc/ssh/sshd_config`
Make sure to configure as follows
(We only need to allow 'user', but
you can add multiple users as well):

```ssh
PermitRootLogin no
AllowUsers user otheruser yetanotheruser
PasswordAuthentication no
UsePam no
```

Then, restart SSH daemon

```shell
systemctl restart sshd
```

# Enable UFW

```shell
# Allow SSH before enabling,
# Will drop shell otherwise.
ufw allow 22

# Enable firewall
ufw enable
```
