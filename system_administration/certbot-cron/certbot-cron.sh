#!/bin/sh

# Add the domains for which you need an ssl cert
set -- example.org sub.example.org mail.example.org
# Specify argunments for certbot
CERTBOT_OPTS="certonly --standalone --register-unsafely-without-email --agree-tos"

for i in "$@"; do
    certbot -d $i "$CERTBOT_OPTS"
    # The following commands move certs to a preconfigured ejabberd
    # directory, ignore if not using ejabberd.
    #mkdir -p /etc/ejabberd/certs/$i
    #cp /etc/letsencrypt/live/$i/fullchain.pem /etc/ejabberd/certs/$i
    #cp /etc/letsencrypt/live/$i/privkey.pem /etc/ejabberd/certs/$i
done
