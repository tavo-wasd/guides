# How does it work?

By default, network interfaces will be automatically configured by
the Debian installer in `/etc/network/interfaces`.
There is a way to "call" a `wpa_supplicant.conf` configuration
file in `/etc/network/interfaces` to handle
_"accepted networks and security policies..."_ (WPA_SUPPLICANT.CONF(5)),
using `wpasupplicant`.

Out of the box, Debian GNU/Linux 12 (as of writing) comes with the `wpasupplicant` package installed.
So, you can connect to a wireless network without any extra software.
This package gives access to the `wpa_passphrase` command to any user, and
`wpa-action`, `wpa-cli` and `wpa_supplicant` commands to the root user,
the _latter_ needs to be ran using a **root shell**, prepending `sudo` won't work.
However, there is a way to configure new networks without `sudo`
using `wpa_gui` as explained later.

# How to properly configure wireless networks

## Add `wpa-roam` command to `/etc/network/interfaces`

Firstly, remove any dhcp configured interfaces as they
must be configured with manual mode for
`wpa-roam` to work, the file `/etc/network/interfaces`:

```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug wlo1
iface wlo1 inet dhcp
    wpa-ssid ExampleNetwork
    wpa-psk foob4r123
```

Should be modified to look like this (comments can be ignored):

```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
allow-hotplug wlo1
iface wlo1 inet manual
#  *Removed previously configured network credentials,
#  *and set the interface to manual mode

#  *This will allow wpa_supplicant
wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
iface default inet dhcp
```

**Note**: Make sure this file is not readable by running:
```shell
sudo chown -R root:root /etc/network/interfaces
sudo chmod 600 /etc/network/interfaces
```


## Configure `wpa_supplicant`

There are example files in `/usr/share/doc/wpa_supplicant/examples/`,
`wpa_supplicant.conf` possibly being the most useful.
However, this is the configuration that I personally use
for `/etc/wpa_supplicant/wpa_supplicant.conf`:

```
# This allows using wpa_cli
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev

# Allow wpa_supplicant to update configuration
# For example when adding a new network
update_config=1

# MAC Address randomization
mac_addr=1
preassoc_mac_addr=1
gas_rand_mac_addr=1

network={
        ssid="ExampleNetwork"
        psk="foob4r123"
}
```

The `network{}` block shown above can be generated using
`wpa_passphrase`, for example:

```shell
wpa_passphrase "ExampleNetwork" "foob4r123"
```

Will output:

```
network={
        ssid="ExampleNetwork"
        #psk="foob4r123"
        psk=07765d3708cbabec40812cb1abaac936b420d6a0d951dd94dde372f96c86455f
}
```

Which then can be appended to the `/etc/wpa_supplicant/wpa_supplicant.conf` file.

**Note**: The `#psk=` line can be omitted for the hashed password.
However, this will not prevent others with access to this hash
to connect to the network, but at least it will not reveal the
plain text password if the system is compromised.

**Note**: Make sure this file is not readable by running:

```shell
sudo chown -R root:root /etc/wpa_supplicant/wpa_supplicant.conf
sudo chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf
```

## Tips

Now you can use `sudo wpa_cli` to configure networks
using the command line, see WPA_CLI(8).

However, `wpagui` is a very small program
that can ease the process of adding new networks.

**Note**: Go ahead and add your user to the netdev group
with `usermod -a -G netdev youruser`

More interestingly it can re-configure
`/etc/wpa_supplicant/wpa_supplicant.conf` with
newly added networks without super user privileges.

**Note**: Sometimes it can make `/etc/wpa_supplicant/wpa_supplicant.conf`
readable, to restore it: `sudo chmod 600 /etc/wpa_supplicant/wpa_supplicant.conf`
will suffice.
