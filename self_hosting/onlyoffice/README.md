# Add non-free and contrib

Edit `/etc/apt/sources.list` and make sure to have enabled **non-free** and **contrib**.

```
deb http://deb.debian.org/debian/ bookworm main non-free-firmware non-free contrib
```

# Install dependencies
```shell
apt install -y nginx nginx-extras postgresql rabbitmq-server redis gnupg libcurl4 libxml2 fonts-dejavu fonts-liberation ttf-mscorefonts-installer fonts-crosextra-carlito fonts-takao-gothic fonts-opensymbol
```

# OnlyOffice configuration
```shell
echo onlyoffice-documentserver onlyoffice/ds-port select 81 | sudo debconf-set-selections
sudo -i -u postgres psql -c "CREATE USER onlyoffice WITH PASSWORD 'DB_PASSWORD';"
sudo -i -u postgres psql -c "CREATE DATABASE onlyoffice OWNER onlyoffice;"
```

# OnlyOffice Docs repository
```shell
mkdir -p -m 700 ~/.gnupg
curl -fsSL https://download.onlyoffice.com/GPG-KEY-ONLYOFFICE | gpg --no-default-keyring --keyring gnupg-ring:/tmp/onlyoffice.gpg --import
chmod 644 /tmp/onlyoffice.gpg
chown root:root /tmp/onlyoffice.gpg
mv /tmp/onlyoffice.gpg /usr/share/keyrings/onlyoffice.gpg

echo "deb [signed-by=/usr/share/keyrings/onlyoffice.gpg] https://download.onlyoffice.com/repo/debian squeeze main" | sudo tee /etc/apt/sources.list.d/onlyoffice.list
apt-get update

apt install onlyoffice-documentserver # Will ask for DB_PASSWORD
```

# Nginx reverse proxy & HTTPS

```shell
curl -L reverse-proxy.tavo.one | sed "s/example.domain.xyz/YOURDOMAIN/g" | tee /etc/nginx/sites-available/onlyoffice.conf 
ln -s /etc/nginx/sites-available/onlyoffice.conf /etc/nginx/sites-enabled/

systemctl restart nginx
certbot -d YOURDOMAIN --nginx --register-unsafely-without-email --agree-tos
```
