# Collabora Online

Pretty office suite with mobile device support.
Based on libreoffice.

# Installation (use with nextcloud)

This installation procedure could serve as a reference for
other purposes, but it is made with nextcloud integration
in mind.

## Import signing keys and setup repository

```shell
wget -qO /usr/share/keyrings/collaboraonline-release-keyring.gpg https://collaboraoffice.com/downloads/gpg/collaboraonline-release-keyring.gpg
echo 'deb [signed-by=/usr/share/keyrings/collaboraonline-release-keyring.gpg arch=amd64] https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-debian11 ./' | tee /etc/apt/sources.list.d/collabora.list
apt update
```

## Install packages

Firstly, install Microsoft fonts such as Arial, Times, etc. (optional)
Collabora's installation will look for system fonts and generate a
systemplate, so, this will ensure MS fonts are available in collabora.

```shell
apt install ttf-mscorefonts-installer
```

Install Collabora front and backend packages, plus spell checking

```shell
apt install coolwsd code-brand hunspell collaboraoffice*-dict-*
```

## Configure nginx

Add to `/etc/nginx/sites-available/collabora.conf`
(change office.example.org with your domain):

```nginx
server {
    listen 80;
    server_name office.example.org;

    # static files
    location ^~ /browser {
        proxy_pass http://127.0.0.1:9980;
        proxy_set_header Host $http_host;
    }

    # WOPI discovery URL
    location ^~ /hosting/discovery {
        proxy_pass http://127.0.0.1:9980;
        proxy_set_header Host $http_host;
    }

    # Capabilities
    location ^~ /hosting/capabilities {
        proxy_pass http://127.0.0.1:9980;
        proxy_set_header Host $http_host;
    }

    # main websocket
    location ~ ^/cool/(.*)/ws$ {
        proxy_pass http://127.0.0.1:9980;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $http_host;
        proxy_read_timeout 36000s;
    }

    # download, presentation and image upload
    location ~ ^/(c|l)ool {
        proxy_pass http://127.0.0.1:9980;
        proxy_set_header Host $http_host;
    }

    # Admin Console websocket
    location ^~ /cool/adminws {
        proxy_pass http://127.0.0.1:9980;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
        proxy_set_header Host $http_host;
        proxy_read_timeout 36000s;
    }
}
```

Enable nginx site.

```
ln -s /etc/nginx/sites-available/collabora.conf /etc/nginx/sites-enabled/
```

Run this and select your domain.

```
certbot --nginx
```

Restart nginx to apply

```
systemctl restart nginx
```

## Edit ``/etc/coolwsd/coolwsd.xml``

### Languages

Add or remove languages, for example, here I set only `es_ES en_US de_DE fr_FR`.
This can impact performance, aim for few languages.

```xml
...
<allowed_languages desc="List of supported languages of Writing Aids (spell checker, grammar checker, thesaurus, hyphenation) on this instance. Allowing too many has negative effect on startup performance." default="de_DE en_GB en_US es_ES fr_FR it nl pt_BR pt_PT ru">es_ES en_US de_DE fr_FR</allowed_languages>
...
```

### SSL Termination

Configure SSL using reverse proxy, secure and fast.

Here I set SSL to false:

```xml
...
<ssl desc="SSL settings">
        <!-- switches from https:// + wss:// to http:// + ws:// -->
        <enable type="bool" desc="Controls whether SSL encryption between coolwsd and the network is enabled (do not disable for production deployment). If default is false, must first be compiled with SSL support to enable." default="true">false</enable> 
...
```

Here I set termination to true:

```xml
...
<!-- SSL off-load can be done in a proxy, if so disable SSL, and enable termination below in production -->
<termination desc="Connection via proxy where coolwsd acts as working via https, but actually uses http." type="bool" default="true">true</termination>
...
```

## Enable coolwsd

```shell
systemctl enable --now coolwsd
```
