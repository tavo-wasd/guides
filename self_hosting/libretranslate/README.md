# LibreTranslate

## Install libretranslate

Installing pipx, then, adding an unprivileged
user 'libretranslate' that will install libretranslate
in its $HOME directory.

```shell
apt install pipx
useradd -m -s /bin/sh libretranslate
sudo -i -u libretranslate pipx install libretranslate
```

## Run libretranslate

Run for the first time to install languages, stop whenever
you want, or sin this at any time to update or finishing Installing
models.

```shell
sudo -i -u libretranslate /home/libretranslate/.local/bin/libretranslate --update-models
```

## Nginx

Add to `/etc/nginx/sites-available/libretranslate.conf`

```nginx
server {
	listen 80;
	listen [::]:80;

	server_name translate.example.org;

	location / {
	    proxy_pass http://localhost:5000/;
	}
}
```

Enable the site

```
ln -s /etc/nginx/sites-available/libretranslate.conf /etc/nginx/sites-enabled/
```

Generate a certificate, select your domain.

```
certbot --nginx
```

Restart nginx

```
systemctl restart nginx
```

## Enable as a service

Create: `/etc/systemd/system/libretranslate.service`.

```systemd
[Unit]
Description=LibreTranslate - Free and Open Source Machine Translation API
After=network.target

[Service]
User=libretranslate
Group=libretranslate
WorkingDirectory=/home/libretranslate
Environment="PATH=/home/libretranslate/.local/pipx/venvs/libretranslate/bin"
ExecStart=/home/libretranslate/.local/bin/libretranslate
Restart=always
ExecReload=/bin/kill -s HUP $MAINPID
KillMode=mixed
TimeoutStopSec=1

[Install]
WantedBy=multi-user.target
```

```shell
systemctl enable --now libretranslate
```

## Done

Your own translation website and API should be
available through `translate.exmaple.org` (the server_name
set in `/etc/nginx/sites-available/libretranslate.conf`)
