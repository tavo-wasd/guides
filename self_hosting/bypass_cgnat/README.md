# Bypass CGNAT
Setup a wireguard VPN to route traffic from a VPS to your local machine.
Based from [this](https://github.com/mochman/Bypass_CGNAT) guide.

### Usage

This creates a file `client.conf` in the current directory,
and it will output the client configuration to the terminal as well.

```shell
wget -qO /opt/bypasscgnat.sh "https://gitlab.com/tavo-wasd/guides/-/raw/main/self_hosting/bypass_cgnat/bypasscgnat.sh"
chmod +x /opt/bypasscgnat.sh
/opt/bypasscgnat.sh
```
