#!/bin/sh

VPSINT="INTERFACE"

TCP=$(cat /opt/wireguard/ports-tcp.txt)
: "${tcpports:=$TCP}"
for port in $tcpports; do
    iptables -A FORWARD -i "$VPSINT" -o wg0 -p tcp --syn --dport "$port" -m conntrack --ctstate NEW -j ACCEPT
    iptables -t nat -A PREROUTING -i "$VPSINT" -p tcp --dport "$port" -j DNAT --to-destination 10.0.0.2
    iptables -t nat -A POSTROUTING -o wg0 -p tcp --dport "$port" -d 10.0.0.2 -j SNAT --to-source 10.0.0.1
    ufw allow "$port"/tcp
done

UDP=$(cat /opt/wireguard/ports-udp.txt)
: "${udpports:=$UDP}"
for port in $udpports; do
    iptables -A FORWARD -i "$VPSINT" -o wg0 -p udp --dport "$port" -m conntrack --ctstate NEW -j ACCEPT
    iptables -t nat -A PREROUTING -i "$VPSINT" -p udp --dport "$port" -j DNAT --to-destination 10.0.0.2
    iptables -t nat -A POSTROUTING -o wg0 -p udp --dport "$port" -d 10.0.0.2 -j SNAT --to-source 10.0.0.1
    ufw allow "$port"/udp
done

