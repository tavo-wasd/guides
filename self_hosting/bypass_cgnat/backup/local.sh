#!/bin/sh
# Local utility for forwarding traffic through wireguard VPN
#
# Some code was borrowed from:
# https://github.com/mochman/Bypass_CGNAT
#                 and
# https://www.youtube.com/watch?v=9tDeh9mutmI
perms=$(umask)

print() {
    printf '\033[1m    %b\033[0m%s\n\n' "$1"
}

# Install wireguard
printf "\nContinue ONLY is wireguard is installed [y/N] " && read -r check
[ "$check" != "y" ] && exit

# Generate wireguard keys
print "Generating wireguard configs..."
sudo mkdir -p /etc/wireguard/
umask 077 # perms for /etc/wireguard/* files
printf "[Interface]\nPrivateKey = " | sudo tee /etc/wireguard/wg0.conf > /dev/null
printf "\nCopy this key to server's setup (Press ENTER when done):\n> "
sudo wg genkey | sudo tee -a /etc/wireguard/wg0.conf | wg pubkey | sudo tee /etc/wireguard/publickey
read -r NULL

# Make wireguard configuration with keys and write to /etc/wireguard/wg0.conf
printf "\nEnter the generated Publickey from server:\n> " && read -r PUBKEY
printf "\nEnter the VPS's public IP:\n> " && read -r VPSIP
printf "Address = 10.0.0.2

[Peer]
PublicKey = $PUBKEY
AllowedIPs = 10.0.0.1/32
Endpoint = $VPSIP:55107
PersistentKeepalive = 25
" | sudo tee -a /etc/wireguard/wg0.conf
umask $perms # restore perms

# Start service
#sudo systemctl start wg-quick@wg0
printf "\nRun 'systemctl enable wg-quick@wg0' so the service starts automatically on boot.\n"
printf "\nDone!\n"
