#!/bin/sh
# Server utility for forwarding traffic through wireguard VPN
#
# Some code was borrowed from:
# https://github.com/mochman/Bypass_CGNAT
#                 and
# https://www.youtube.com/watch?v=9tDeh9mutmI
perms=$(umask)

print() {
    printf '\033[1m    %b\033[0m%s\n\n' "$1"
}

# Enable forwarding FIX
print "Enabling ip_forward..."
cat /etc/sysctl.conf | sed "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g" | sudo tee /etc/sysctl.conf
sudo sysctl -p
sudo sysctl --system

# Install wireguard
print "Installing wireguard..."
sudo apt-get install wireguard -y # Install wireguard
ip a

# Prompt for required fields
printf "\nEnter the VPS's public IP interface (see with 'ip a'):\n> " && read -r VPSINT

# Generate keys and write configuration to /etc/wireguard/wg0.conf
print "Generating wireguard configs..."
sudo mkdir -p /etc/wireguard/
umask 077 # perms for /etc/wireguard/* files
printf "[Interface]\nPrivateKey = " | sudo tee /etc/wireguard/wg0.conf > /dev/null
sudo wg genkey | sudo tee -a /etc/wireguard/wg0.conf | wg pubkey | sudo tee /etc/wireguard/publickey
printf "\nEnter the generated /etc/wireguard/publickey from local machine:\n> " && read -r PUBKEY
printf "ListenPort = 55107
Address = 10.0.0.1/24

PostUp = /opt/wireguard/iptables-up.sh
PostDown = /opt/wireguard/iptables-down.sh

[Peer]
PublicKey = $PUBKEY
AllowedIPs = 10.0.0.2/32
" | sudo tee -a /etc/wireguard/wg0.conf
printf "\nCopy this key to local machine's setup:\n> "
sudo cat /etc/wireguard/publickey
read -r NULL
umask $perms # restore perms

# Start service
print "Starting wg-quick@wg0..."
sudo ufw allow 55107
sudo systemctl start wg-quick@wg0

# iptables configuration
print "Configuring iptables & ufw..."
sudo iptables -P FORWARD DROP
sudo iptables -A FORWARD -i "$VPSINT" -o wg0 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A FORWARD -i wg0 -o "$VPSINT" -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

sudo ufw status | grep -q "active" && sudo ufw reload

print "\nDone!\n"
printf "Run 'systemctl enable wg-quick@wg0' so the service starts automatically on boot.\n\n"
