#!/bin/sh
TCP="80 443"
UDP="80 443"

iptables_setup() {
    MODE="$1"
    SERVER_INT="$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)"
    WG_SERVER_IP="10.0.0.1"
    WG_CLIENT_IP="10.0.0.2"
    : "${tcpports:=$TCP}"
    : "${udpports:=$UDP}"

    if [ "$MODE" = "up" ] ; then
        for port in $tcpports; do
            iptables -A FORWARD -i "$SERVER_INT" -o wg0 -p tcp --syn --dport "$port" -m conntrack --ctstate NEW -j ACCEPT
            iptables -t nat -A PREROUTING -i "$SERVER_INT" -p tcp --dport "$port" -j DNAT --to-destination "$WG_CLIENT_IP"
            iptables -t nat -A POSTROUTING -o wg0 -p tcp --dport "$port" -d "$WG_CLIENT_IP" -j SNAT --to-source "$WG_SERVER_IP"
        done
        for port in $udpports; do
            iptables -A FORWARD -i "$SERVER_INT" -o wg0 -p udp --dport "$port" -m conntrack --ctstate NEW -j ACCEPT
            iptables -t nat -A PREROUTING -i "$SERVER_INT" -p udp --dport "$port" -j DNAT --to-destination "$WG_CLIENT_IP"
            iptables -t nat -A POSTROUTING -o wg0 -p udp --dport "$port" -d "$WG_CLIENT_IP" -j SNAT --to-source "$WG_SERVER_IP"
        done
    fi

    if [ "$MODE" = "down" ] ; then
        for port in $tcpports; do
            iptables -D FORWARD -i "$SERVER_INT" -o wg0 -p tcp --syn --dport "$port" -m conntrack --ctstate NEW -j ACCEPT
            iptables -t nat -D PREROUTING -i "$SERVER_INT" -p tcp --dport "$port" -j DNAT --to-destination "$WG_CLIENT_IP"
            iptables -t nat -D POSTROUTING -o wg0 -p tcp --dport "$port" -d "$WG_CLIENT_IP" -j SNAT --to-source "$WG_SERVER_IP"
        done
        for port in $udpports; do
            iptables -D FORWARD -i "$SERVER_INT" -o wg0 -p udp --dport "$port" -m conntrack --ctstate NEW -j ACCEPT
            iptables -t nat -D PREROUTING -i "$SERVER_INT" -p udp --dport "$port" -j DNAT --to-destination "$WG_CLIENT_IP"
            iptables -t nat -D POSTROUTING -o wg0 -p udp --dport "$port" -d "$WG_CLIENT_IP" -j SNAT --to-source "$WG_SERVER_IP"
        done
    fi
}

[ "$1" = "up" ] && iptables_setup up && exit 0
[ "$1" = "down" ] && iptables_setup down && exit 0

install_wireguard() {
    apt-get install -y wireguard iptables
    mkdir -p /etc/wireguard
    chmod 600 -R /etc/wireguard/
}

wireguard_config() {
    ufw allow 55107

    printf "net.ipv6.conf.all.forwarding = 1\nnet.ipv4.ip_forward = 1" > /etc/sysctl.d/wg.conf
    sysctl -p
    sysctl --system

    SERVER_IP4="$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)"
    SERVER_PRV_KEY=$(wg genkey)
    SERVER_PUB_KEY=$(echo "${SERVER_PRV_KEY}" | wg pubkey)

    CLIENT_PRV_KEY=$(wg genkey)
    CLIENT_PUB_KEY=$(echo "${CLIENT_PRV_KEY}" | wg pubkey)
    CLIENT_PRE_KEY=$(wg genpsk)
}

install_wireguard
wireguard_config
ufw reload

echo "[Interface]
PrivateKey = ${SERVER_PRV_KEY}
ListenPort = 55107
Address = 10.0.0.1/24 

PostUp = /opt/bypasscgnat.sh up
PostDown = /opt/bypasscgnat.sh down

[Peer]
PublicKey = ${CLIENT_PUB_KEY}
PresharedKey = ${CLIENT_PRE_KEY}
AllowedIPs = 10.0.0.2/32" > /etc/wireguard/wg0.conf

printf '\n\033[1m\033[34m=== client configuration ===\033[0m\n\n'

echo "[Interface]
PrivateKey = ${CLIENT_PRV_KEY}
Address = 10.0.0.2

[Peer]
PublicKey = ${SERVER_PUB_KEY}
PresharedKey = ${CLIENT_PRE_KEY}
AllowedIPs = 10.0.0.1/32
Endpoint = ${SERVER_IP4}:55107
PersistentKeepalive = 25" | tee client.conf

echo
