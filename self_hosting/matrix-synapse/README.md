# Matrix Synapse

```
sudo -i -u postgres psql -c "CREATE USER synapse_user WITH PASSWORD 'DB_PASSWORD';"
sudo -i -u postgres psql -c "CREATE DATABASE synapse ENCODING 'UTF8' LC_COLLATE='C' LC_CTYPE='C' template=template0 OWNER synapse_user;"
systemctl restart postgresql
```

## Install Synapse

```
apt install -y lsb-release wget apt-transport-https
wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/matrix-org.list
apt update
apt install matrix-synapse
```

## Configure nginx
