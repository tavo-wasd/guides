# Install dependencies

```shell
apt install -y curl nginx python3-certbot-nginx postgresql php php-{fpm,bcmath,bz2,intl,gd,mbstring,pgsql,zip,xml,curl,imagick,gmp} libmagickcore-6.q16-6-extra
```

For some reason that installs apache2, remove it as we don't need it.

```shell
apt purge apache2 -y ; apt autoremove -y
```

### Check PHP version...

As of 2023-07-10, it's 8.2.
You can also check with `ls /etc/php` by checking the version's folder name.

```shell
php -v | grep -o '[0-9].*(cli)' | cut -d '.' -f 1,2
```

# Set up postgresql database

```shell
systemctl enable --now postgresql
su -l postgres
postgres$ psql -c "CREATE USER nextcloud WITH PASSWORD 'DB_PASSWORD';"
postgres$ psql -c "CREATE DATABASE nextcloud TEMPLATE template0 ENCODING 'UTF8';"
postgres$ psql -c "ALTER DATABASE nextcloud OWNER TO nextcloud;"
postgres$ psql -c "GRANT ALL PRIVILEGES ON DATABASE nextcloud TO nextcloud;"
postgres$ psql -c "GRANT ALL PRIVILEGES ON SCHEMA public TO nextcloud;"
postgres$ exit
```

# Nginx configuration & cert
I modified this config to be easily set up with SSL via certbot,
but you can visit the following url for the official example:
[Nextcloud's nginx example configuration](https://docs.nextcloud.com/server/19/admin_manual/installation/nginx.html).
```shell
certbot certonly --nginx --register-unsafely-without-email --agree-tos -d YOURDOMAIN
curl -L nextcloud-nginx.tavo.one | sed "s/cloud.example.com/YOURDOMAIN/g" | tee /etc/nginx/sites-available/nextcloud.conf 
ln -s /etc/nginx/sites-available/nextcloud.conf /etc/nginx/sites-enabled/
systemctl restart nginx
```

# Download & extract latest nextcloud
```shell
wget -O /opt/nextcloud-$(date -I).tar.bz2 https://download.nextcloud.com/server/releases/latest.tar.bz2
tar -xjf /opt/nextcloud-$(date -I).tar.bz2 -C /var/www
```

# Permissions for www-data user & restart services
```shell
chown -R www-data:www-data /var/www/nextcloud
chmod -R 755 /var/www/nextcloud
systemctl enable --now php8.2-fpm
systemctl enable --now nginx
```

# Recommended modifications:

RECOMMENDED MODIFICATIONS TO ENHANCE
SECURITY & PERFORMANCE:

I find it better to configure them
one by one and troubleshoot each one
separately that way!

## Tune php-fpm

Recommended settings for `/etc/php/8.2/fpm/pool.d/www.conf`
This should get rid of the _'getenv("PATH") only returns an empty response'_
overview panel notification, plus enhance performance.

```php
pm = dynamic
pm.max_children = 120
pm.start_servers = 12
pm.min_spare_servers = 6
pm.max_spare_servers = 18

env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
```

Recommended settings for `/etc/php/$php_ver/fpm/php.ini`
Sometimes when installing apps, an error might occur
and it's not always verbose as to why it can't proceed.
This could fix some of those problems.

```php
opcache.interned_strings_buffer=32

memory_limit = 512M
```

By default (at least on Debian 12), this warning will come up
on the Recording backend section under Administration settings>Talk:
_"The PHP settings `upload_max_filesize` or `post_max_size`
only will allow to upload files up to 2 MiB."_
Here I allow 8GB recordings but use whatever size you prefer.

```php
upload_max_filesize = 8192M
post_max_size = 8192M
```

## Tune memcache

### APCu local memcache
For performance, documentation suggests enabling a
memcache, for 'local memcache', APCu is recommended.

Install dependencies

```shell
apt install php-apcu
```

Add the following lines to `/var/www/nextcloud/config/config.php` under `$CONFIG=array`...

```php
'memcache.local' => '\OC\Memcache\APCu',
```

In order to avoid problems with cron jobs,
Nextcloud's docs suggest to add these  
somewhere in `php.ini`, but I found it
worked best when I added them in 
`/etc/php/$php_ver/mods-available/apcu.ini`
(extension=apcu.so should already be there):

```php
extension=apcu.so
apc.enabled=1
apc.enable_cli=1
```

Restart php$php_ver-fpm
```shell
systemctl restart php8.2-fpm
```

### Redis server (Transactional file locking)
This is enabled by default in nextcloud to prevent
dataloss with failed uploads or disconnects, however,
it loads your database greatly for this purpose.
To delegate this processing to redis, docs recommend:

NOTE:
If you run into trouble just remove the
`memcache.locking' => '\OC\Memcache\Redis'`,
line mentioned below.

Insall required pacakges

```shell
apt install redis-server php-redis
```

Uncomment & modify `/etc/redis/redis.conf`

```
port 0
unixsocket /run/redis/redis.sock
unixsocketperm 770
```

Permissions, enable & run redis

```shell
sudo usermod -a -G redis www-data
systemctl enable --now redis-server
```

Add the following lines to `/var/www/nextcloud/config/config.php` under `$CONFIG=array`...

```php
'filelocking.enabled' => true,
'memcache.locking' => '\OC\Memcache\Redis',
'redis' => array(
     'host' => '/var/run/redis/redis.sock',
     'port' => 0,
     'timeout' => 0.0,
      ),
```


(Optional) For distributed cache, also add:
`memcache.distributed' => '\OC\Memcache\Redis'`,

Similarly with APCu, it is recommended
to add these settings in php.ini, I just
find it cleaner in the respective module's
config in `/etc/php/$php_ver/mods-available/redis.ini`
(extension=redis.so should already be there):

```php
extension=redis.so
redis.session.locking_enabled=1
redis.session.lock_retries=-1
redis.session.lock_wait_time=10000
```

```shell
Restart services
systemctl restart redis-server
systemctl restart php$php_ver-fpm
systemctl restart nginx
```

## Cron for background jobs

Run the following command:

```shell
crontab -u www-data -e
```

Append the following line AS IS
to the end of the document
(as recommended in nextcloud docs):

```
*/5  *  *  *  * php -f /var/www/nextcloud/cron.php
```

```shell
systemctl restart php8.2-fpm
```

## Apply
If applied, restart to apply changes with:

```shell
systemctl restart redis-server
systemctl restart php$php_ver-fpm
systemctl restart nginx
```
