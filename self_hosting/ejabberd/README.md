# Enable ports

These are the ports needed for ejabberd to work.
Ports 80 and 443 are needed for deploying and SSL certificate with certbot.
Read more about ports in [ejabberd's docs](https://docs.ejabberd.im/admin/guide/security),
to see which ports are needed for whatever modules you need.

```shell
set -- 80 443 5222 5223 5269 5280 5443 1883 8883 3478 5349 7777
for port in "$@"; do ufw allow "$port" ; done
ufw reload
```

# Download ejabberd

Debian conveniently offers the packages in the official repositories!

```shell
apt update
apt install ejabberd python3-certbot erlang-p1-pgsql
systemctl enable --now ejabberd
```

# Generate certs

This is a slightly modified snippet from [Nerd on the Street](https://github.com/nerdonthestreet).
Change the example.org to your preference.

```shell
#!/bin/sh

set -- example.org conference.example.org proxy.example.org pubsub.example.org upload.example.org stun.example.org turn.example.org
CERTBOT_OPTS="certonly --standalone --register-unsafely-without-email --agree-tos"

for i in "$@"; do
    certbot -d $i "$CERTBOT_OPTS"
    mkdir -p /etc/ejabberd/certs/$i
    cp /etc/letsencrypt/live/$i/fullchain.pem /etc/ejabberd/certs/$i
    cp /etc/letsencrypt/live/$i/privkey.pem /etc/ejabberd/certs/$i
done
```

# Directories and permissions

```shell
chown -R ejabberd:ejabberd /etc/ejabberd/certs
mkdir -p /var/www/upload
chown -R ejabberd:ejabberd /var/www/upload
```

# Base configuration

Fill with your domain.

```yaml
hosts:
  - example.org
```

Enable the path for previously generated certs.

```yaml
certfiles:
  - "/etc/ejabberd/certs/*/*.pem"
```

Note: Remember to `systemctl restart ejabberd` each time you modify `/etc/ejabberd.yml`.

# Example for some configs (optional)

## Admin user

Register admin user.

```shell
su -c "ejabberdctl register admin example.org ADMIN_PASSWORD" ejabberd
systemctl restart ejabberd
```

Enable admin user.

```yaml
acl:
  admin:
    user: admin
```

## Message archive and http upload

Uncomment for compliance with XMPP standards.

```yaml
mod_http_upload:
    put_url: https://@HOST@:5443/upload
    docroot: /var/www/upload
    custom_headers:
      "Access-Control-Allow-Origin": "https://@HOST@"
      "Access-Control-Allow-Methods": "GET,HEAD,PUT,OPTIONS"
      "Access-Control-Allow-Headers": "Content-Type"
```

Enable message archive.

```yaml
mod_mam:
  assume_mam_usage: true
  default: always
```

## Postgresql database

Set postgresql as database. WARNING. I can't get this to work.
Don't bother if you have few users anyway.

```shell
sudo -i -u postgres psql -c "CREATE USER ejabberd WITH PASSWORD 'DB_PASSWORD';"
sudo -i -u postgres psql -c "CREATE DATABASE ejabberd OWNER ejabberd;"
su -c "curl -s https://raw.githubusercontent.com/processone/ejabberd/master/sql/pg.sql | psql ejabberd" postgres
```

```yaml
sql_type: pgsql
sql_server: "localhost"
sql_database: "ejabberd"
sql_username: "ejabberd"
sql_password: "DB_PASSWORD"
default_db: sql
```

## Call/Videocall support

Enable this module

```yaml
ejabberd_stun:
```

## Registration

Enable registration in your server

```yaml
mod_register:
```

Otherwise registered via:

```shell
su -c "ejabberdctl register USERNAME example.org USER_PASSWORD" ejabberd
```

Note: Remember to `systemctl restart ejabberd` each time you modify `/etc/ejabberd.yml`.
