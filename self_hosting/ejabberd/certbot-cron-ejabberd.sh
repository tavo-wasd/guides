#!/bin/sh

set -- example.org conference.example.org proxy.example.org pubsub.example.org upload.example.org stun.example.org turn.example.org
CERTBOT_OPTS="certonly --standalone --register-unsafely-without-email --agree-tos"

for i in "$@"; do
    certbot -d $i "$CERTBOT_OPTS"
    mkdir -p /etc/ejabberd/certs/$i
    cp /etc/letsencrypt/live/$i/fullchain.pem /etc/ejabberd/certs/$i
    cp /etc/letsencrypt/live/$i/privkey.pem /etc/ejabberd/certs/$i
done

chown -R ejabberd:ejabberd /etc/ejabberd/certs
