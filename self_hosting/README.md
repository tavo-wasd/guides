# self-hosted

## Description
Aims to be a comprehensible guide to a self-hosted, FOSS digital presence.

## Roadmap
- Fininsh scripting the Bypass CGNAT part.
- Script the deployment of common apps for reproducibility.

## Authors and acknowledgment

I used these sources as a guide, documented steps and added useful configurations.

[mochman@github](https://github.com/mochman) - [Bypass CGNAT](https://github.com/mochman/Bypass_CGNAT)

[nerdonthestreet@github](https://github.com/nerdonthestreet) - [Install ONLYOFFICE on a Nextcloud Server](https://www.youtube.com/watch?v=Gc1zUmrxtKE)

[DenshiVideo](https://wiki.denshi.org/hypha/server/ejabberd) - [How to setup an XMPP server (ejabberd)](https://www.youtube.com/watch?v=rY0kRSj2rmU)

[Matthew Evan](https://github.com/MattMadness) - [landchad.net/nextcloud](https://landchad.net/nextcloud/)

## Project status
Very early stages, doesn't work, use as a guide for manual implementation.
